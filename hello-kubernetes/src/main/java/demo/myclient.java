package demo;

import com.derbysoft.servreg.client.ClientBuilder;
import com.derbysoft.servreg.client.api.ClientErrorResult;
import com.derbysoft.servreg.client.api.ServiceRegistryClient;
import com.derbysoft.servreg.model.*;
import com.derbysoft.servreg.registry.ServiceRegistryBuilder;
import com.derbysoft.servreg.registry.common.ServiceProfileHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class myclient {

    private static final Logger log= Logger.getLogger("k8s-demo");

    private static final String SERVICE_ID_FORMAT = "%s:us-west-2::test::";

    public static final String DEFAULT_SERVICE_ID = String.format(SERVICE_ID_FORMAT, "test");

    public static void main(String[] args) {

        ServiceRegistryClient defaultServiceRegistryClient = ClientBuilder.newBuilder()
                .withRegistryBuilder(ServiceRegistryBuilder.newBuilder().withAddresses("10.110.206.203:10080"))
                .buildRegistryClient();

        Service service = Service.newBuilder()
                .setServiceId(DEFAULT_SERVICE_ID)
                .setTeam("stone")
                .build();
        List<Endpoint> endpoints = new ArrayList();
        endpoints.add(Endpoint.newBuilder()
                .setEndpointType(EndpointType.INSTANCE_TYPE)
                .setProxyType(ProxyType.Nil)
                .setHost("127.0.0.1")
                .setPort(8080)
                .setPublicIP("192.168.0.1")
                .setWeight(1)
                .setStatus(EndpointStatus.INSERVICE)
                .build());

        ServiceProfileHolder serviceProfileHolder = new ServiceProfileHolder(service, endpoints);

        ClientErrorResult result = defaultServiceRegistryClient.register(serviceProfileHolder);

        if (result.hasError()) {
            System.out.println(result.getError().getErrorMessage());
            log.setLevel(Level.INFO);
            log.info(result.getError().getErrorMessage());
        }



    }

}
